function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    if p == 1
        mp = M
        return mp
    end
    if p > 1
        i = 1
        mp = M
        while i < p
            mp = multiplica(mp, M)
            i += 1
        end
        return mp
    end
end

using Test

function testamatrixpot()
    println("Início dos testes")
    @test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
    @test matrix_pot([1 2 ; 3 4], 2) == [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot([1 2 ; 3 4], 3) == [37.0 54.0 ; 81.0 118.0]
    @test matrix_pot([1 2 ; 3 4], 10) == [4783807.0 6972050.0 ; 10458075.0 15241882.0]
    println("Final dos testes")
end

testamatrixpot()

function matrix_pot_by_squaring(M, p)
    if p == 1
        mp = M
        return mp
    end
    if p > 1
        if p % 2 == 0
            mp = multiplica(M, M)
            return matrix_pot_by_squaring(mp, (p / 2))
        end
        if p % 2 == 1
            mp = multiplica(M, M)
            mp2 = matrix_pot_by_squaring(mp, ((p - 1) / 2))
            return multiplica(M, mp2)
        end
    end
end

function testamatrixpotbysquaring()
    println("Início dos testes")
    @test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 3) == [37.0 54.0 ; 81.0 118.0]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 10) == [4783807.0 6972050.0 ; 10458075.0 15241882.0]
    println("Final dos testes")
end

testamatrixpotbysquaring()

using LinearAlgebra

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)
    @time matrix_pot(M, 100)
    @time matrix_pot_by_squaring(M, 100)
    @time matrix_pot(M, 1000)
    @time matrix_pot_by_squaring(M, 1000)
end

compare_times()
